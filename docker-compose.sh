#!/bin/bash

GID=`id -G | cut -f1 -d ' '`

# Get stack path & move into it.
STACK_PATH=`realpath "$(dirname $0)"`
cd $STACK_PATH

# Check if in stack.
if [[ ! -f docker-compose.yaml ]] ; then
	1>&2 echo "Do not find any docker-compose.yml in the $(dirname $0) directory."
	1>&2 echo "Are you sure you have placed this $0 script inside a stack directory."
	exit 1
fi

# Check if .env.local file exist.
if [[ ! -f .env.local ]] ; then
	1>&2 echo "Do not find any .env.local in the $(dirname $0) directory."
	exit 1
fi

if [ "exec" = "$1" ] ; then
  shift
  docker-compose --env-file=.env.local exec -u $UID:$GID $*
  exit $?
fi

docker-compose --env-file=.env.local $*
exit $?
