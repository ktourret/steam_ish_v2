<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Category>
 *
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function save(Category $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Category $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * Effectue une recherche de type "LIKE %_%" parmi les noms de catégories
     *
     * @param string $name le nom à rechercher parmi les catégories
     * @return array
     */
    public function findAllByApproxName(string $name): array {
        return $this->createQueryBuilder('c')
            ->where('c.name LIKE :name')
            ->setParameter('name', '%'.$name.'%')
            ->getQuery()
            ->getArrayResult();
    }

    public function getMostSoldCategories(int $limit = 3): array
    {
        $rsm = new ResultSetMapping;
        $rsm->addEntityResult(Category::class, 'c');
        $rsm->addFieldResult('c', 'id', 'id');
        $rsm->addFieldResult('c', 'name', 'name');
        $rsm->addFieldResult('c', 'slug', 'slug');

        return $this->_em
            ->createNativeQuery(
                'SELECT c.*
                FROM category c
                JOIN game_category ON game_category.category_id = c.id
                JOIN user_own_game ON user_own_game.game_id = game_category.game_id
                GROUP BY c.id
                ORDER BY COUNT(*) DESC
                LIMIT ?',
                $rsm
            )
            ->setParameter(1, $limit)
            ->getResult();
    }

    public function queryAll(): QueryBuilder
    {
        return $this->createQueryBuilder('category');
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneBySlug(string $slug): ?Category
    {
        return $this->createQueryBuilder('category')
            ->select('category', 'games')
            ->leftJoin('category.games', 'games')
            ->where('category.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findByApproxSearch(string $searched)
    {
        return $this->createQueryBuilder('c')
            ->where('c.name LIKE :searched')
            ->setParameter('searched', '%'.$searched.'%')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

}
