<?php

namespace App\Slugger;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class PrePersistSluggerEventSubscriber implements EventSubscriber
{

    public function __construct(
        private SlugService $slugService
    )
    {
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $eventArgs): void {
        /** @var SlugInterface $object */
        if (($object = $eventArgs->getObject()) instanceof SlugInterface) {
            $object->setSlug($this->slugService->slugify($object->getFields()));
        }
    }

}
