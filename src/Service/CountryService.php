<?php

namespace App\Service;

use App\Entity\Publisher;
use App\Entity\User;
use App\Repository\CountryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Drosalys\Bundle\ApiBundle\Event\PrePersistEvent;
use Symfony\Component\String\Exception\InvalidArgumentException;

class CountryService
{

    public function __construct(
        private CountryRepository $countryRepository,
        private EntityManagerInterface $em
    )
    {
    }

    public function checkExistingCountry(PrePersistEvent $event) {
        $data = $event->getData();

        if ($data instanceof Publisher || $data instanceof User) {
            if ($data->getCountry()->getSlug() !== null) {
                if (null === $country = $this->countryRepository->findOneBy(['slug' => $data->getCountry()->getSlug()])) {
                    throw new InvalidArgumentException('The country you are looking for doesn\'t exists');
                }
                $data->setCountry($country);
            }

            $this->em->persist($data);
            $this->em->flush();
        }
    }

}
