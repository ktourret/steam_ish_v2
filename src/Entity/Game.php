<?php

namespace App\Entity;

use App\Repository\GameRepository;
use App\Slugger\SlugInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: GameRepository::class)]
#[UniqueEntity(fields: 'name', message: 'account.constraints.unique.name')]
class Game implements SlugInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['GameList', 'Game', 'Publisher', 'Genre', 'CommentPost', 'Comment'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['GameList', 'Account', 'Game', 'Publisher', 'Country', 'Genre', 'Comment'])]
    private ?string $name = null;

    #[ORM\Column]
    #[Groups(['GameList', 'Game', 'Publisher', 'Account', 'Country', 'Genre'])]
    private ?float $price = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['GameList', 'Game', 'Publisher'])]
    private ?string $description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['GameList', 'Game', 'Publisher', 'Account', 'Country', 'Genre'])]
    private ?\DateTimeInterface $publishedAt = null;

    #[ORM\Column(length: 255)]
    #[Groups(['GameList', 'Game', 'Publisher', 'Account', 'Country', 'Genre'])]
    private ?string $thumbnailCover = null;

    #[ORM\Column(length: 255)]
    #[Groups(['GameList', 'Account', 'Game', 'Publisher', 'Country', 'Genre', 'Comment'])]
    private ?string $slug = null;

    #[ORM\OneToMany(mappedBy: 'game', targetEntity: Review::class)]
    #[Groups(['Game'])]
    private Collection $reviews;

    #[ORM\ManyToMany(targetEntity: Category::class, inversedBy: 'games')]
    #[Groups(['Game'])]
    private Collection $categories;

    #[ORM\ManyToOne(inversedBy: 'games')]
    #[Groups(['Game'])]
    private ?Publisher $publisher = null;

    #[ORM\ManyToMany(targetEntity: Country::class)]
    #[Groups(['Game'])]
    private Collection $countries;

    public function __construct()
    {
        $this->reviews = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->countries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): static
    {
        $this->price = $price;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(\DateTimeInterface $publishedAt): static
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    public function getThumbnailCover(): ?string
    {
        return $this->thumbnailCover;
    }

    public function setThumbnailCover(string $thumbnailCover): static
    {
        $this->thumbnailCover = $thumbnailCover;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, Review>
     */
    public function getReviews(): Collection
    {
        return $this->reviews;
    }

    public function addReview(Review $review): static
    {
        if (!$this->reviews->contains($review)) {
            $this->reviews->add($review);
            $review->setGame($this);
        }

        return $this;
    }

    public function removeReview(Review $review): static
    {
        if ($this->reviews->removeElement($review)) {
            // set the owning side to null (unless already changed)
            if ($review->getGame() === $this) {
                $review->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Category>
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): static
    {
        if (!$this->categories->contains($category)) {
            $this->categories->add($category);
        }

        return $this;
    }

    public function removeCategory(Category $category): static
    {
        $this->categories->removeElement($category);

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): static
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * @return Collection<int, Country>
     */
    public function getCountries(): Collection
    {
        return $this->countries;
    }

    public function addCountry(Country $country): static
    {
        if (!$this->countries->contains($country)) {
            $this->countries->add($country);
        }

        return $this;
    }

    public function removeCountry(Country $country): static
    {
        $this->countries->removeElement($country);

        return $this;
    }

    public function getFields(): ?string
    {
        return $this->name;
    }
}
