<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use App\Slugger\SlugInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CountryRepository::class)]
#[UniqueEntity(fields: 'name'), UniqueEntity(fields: 'code')]
class Country implements SlugInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['Country', 'CountryList', 'AccountList', 'User', 'Game', 'Publisher'])]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['Country', 'CountryList', 'AccountList', 'User', 'Game', 'Publisher'])]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    #[Groups(['Country', 'CountryList', 'AccountList', 'User', 'Game', 'Publisher'])]
    private ?string $nationality = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['Country', 'CountryList', 'AccountList', 'User', 'Game', 'Publisher'])]
    private ?string $urlFlag = null;

    #[ORM\Column(length: 2)]
    #[Groups(['Country', 'CountryList', 'AccountList', 'User', 'Game', 'Publisher'])]
    private ?string $code = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Groups(['Country', 'CountryList', 'AccountList', 'User', 'Game', 'Publisher'])]
    private ?string $slug = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getNationality(): ?string
    {
        return $this->nationality;
    }

    public function setNationality(string $nationality): static
    {
        $this->nationality = $nationality;

        return $this;
    }

    public function getUrlFlag(): ?string
    {
        return $this->urlFlag;
    }

    public function setUrlFlag(string $urlFlag): static
    {
        $this->urlFlag = $urlFlag;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     * @return Country
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getFields(): ?string
    {
        return $this->name;
    }
}
