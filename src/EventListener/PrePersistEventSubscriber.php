<?php

namespace App\EventListener;

use App\Entity\Country;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class PrePersistEventSubscriber implements EventSubscriber
{

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $eventArgs): void {
        if (($object = $eventArgs->getObject()) instanceof Country) {
            $object->setUrlFlag('https://flagcdn.com/32x24/'.$object->getCode().'.png');
        }
    }

}
