<?php

namespace App\Controller\Api\Publisher;

use App\Entity\Publisher;
use App\Service\CountryService;
use Drosalys\Bundle\ApiBundle\Event\PrePersistEvent;
use Drosalys\Bundle\ApiBundle\Persister\Attributes\PrePersist;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Put;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Deserializable;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class PutAction
{

    public function __construct(
        private CountryService $countryService
    ) { }

    /**
     * Update a Publisher
     * @param Publisher $publisher
     * @return Publisher
     */
    #[Put('/api/publisher/{id}')]
    #[Serializable(groups: 'Publisher'), Deserializable('publisher', groups: 'PublisherPost')]
    #[PrePersist]
    public function __invoke(Publisher $publisher): Publisher
    {
        return $publisher;
    }

    /**
     * @param PrePersistEvent $event
     */
    public function prePersist(PrePersistEvent $event): void {
        $this->countryService->checkExistingCountry($event);
    }

}