<?php

namespace App\Controller\Api\Reviews;

use App\Entity\Review;
use App\Repository\ReviewRepository;
use Doctrine\ORM\QueryBuilder;
use Drosalys\Bundle\ApiBundle\Pagination\Attributes\Paginable;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class CollectionReview
{
    /**
     * CollectionAction constructor.
     * @param ReviewRepository $reviewRepository
     */
    public function __construct(private ReviewRepository $reviewRepository) { }

    /**
     * Get Comment list.
     * @return QueryBuilder
     */
    #[Get('/api/review')]
    #[Serializable(groups: 'Review')]
    #[Paginable(Review::class)]
    public function __invoke(): QueryBuilder
    {
        return $this->reviewRepository->getQbAll();
    }
}
