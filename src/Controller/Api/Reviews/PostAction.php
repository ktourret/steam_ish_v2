<?php


namespace App\Controller\Api\Reviews;


use App\Entity\Review;
use App\Repository\GameRepository;
use App\Repository\UserRepository;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Post;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Deserializable;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;
use Exception;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PostAction.php
 *
 * @author Kevin Tourret
 */
class PostAction
{

    public function __construct(
        private GameRepository      $gameRepository,
        private UserRepository      $userRepository
    )
    {
    }

    /**
     * Create a new Comment
     * @param Request $request
     * @param Review $review
     * @return Review
     * @throws Exception
     */
    #[Post('/api/review')]
    #[Serializable(groups: 'Review'), Deserializable('review', groups: 'ReviewPost')]
    public function __invoke(Request $request, Review $review): Review
    {
        $contentData = json_decode($request->getContent(), true);

        $user = $this->userRepository->findOneBy(['id' => $contentData['user']['id']]);
        $game = $this->gameRepository->findOneBy(['id' => $contentData['game']['id']]);

        if ($user === null || $game === null) {
            throw new Exception('Cannot find the user or the game with the datas sent');
        }

        $review->setUser($user)
            ->setGame($game);

        return $review;
    }

}
