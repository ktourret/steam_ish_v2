<?php


namespace App\Controller\Api\Account;


use App\Entity\User;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Post;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Deserializable;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class PostAction.php
 *
 * @author Kevin Tourret
 */
class PostAction
{

    /**
     * Create a new Account
     * @param User $user
     * @param UserPasswordHasherInterface $passwordHasher
     * @return User
     */
    #[Post('/api/user')]
    #[Serializable(groups: 'User'), Deserializable('user', groups: 'UserPost')]
    public function __invoke(
        User $user,
        UserPasswordHasherInterface $passwordHasher
    ): User
    {
        $user->setPassword(
            $passwordHasher->hashPassword($user, $user->getPassword())
        );
        return $user;
    }

}
