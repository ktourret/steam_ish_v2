<?php


namespace App\Controller\Api\Account;

use App\Entity\User;
use App\Repository\UserRepository;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

/**
 * Class GetAction.php
 *
 * @author Kevin Tourret
 */
class GetAction
{

    /**
     * CollectionAction constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(private UserRepository $userRepository) { }

    /**
     * Get User account by slug
     * @param string $slug
     * @return User|null
     */
    #[Get('/api/user/{slug}')]
    #[Serializable(groups: 'User')]
    public function __invoke(string $slug): ?User
    {
        return $this->userRepository->findOneBySlug($slug);
    }

}
