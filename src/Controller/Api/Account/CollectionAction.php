<?php

namespace App\Controller\Api\Account;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\QueryBuilder;
use Drosalys\Bundle\ApiBundle\Pagination\Attributes\Paginable;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class CollectionAction
{
    /**
     * CollectionAction constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(private UserRepository $userRepository) { }

    /**
     * Get User account list.
     * @return QueryBuilder
     */
    #[Get('/api/user')]
    #[Serializable(groups: 'UserList')]
    #[Paginable(User::class)]
    public function __invoke(): QueryBuilder
    {
        return $this->userRepository->getQbAll();
    }

}
