<?php


namespace App\Controller\Api\Account;


use App\Entity\User;
use App\Service\CountryService;
use Drosalys\Bundle\ApiBundle\Event\PrePersistEvent;
use Drosalys\Bundle\ApiBundle\Persister\Attributes\PrePersist;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Put;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Deserializable;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

/**
 * Class PutAction.php
 *
 * @author Kevin Tourret
 */
class PutAction
{

    public function __construct(
        private CountryService $countryService
    ) { }

    /**
     * Update an Account
     * @param User $user
     * @return User
     */
    #[Put('/api/user/{id}')]
    #[Serializable(groups: 'User'), Deserializable('user', groups: 'UserPut')]
    #[PrePersist]
    public function __invoke(User $user): User
    {
        return $user;
    }

    /**
     * @param PrePersistEvent $event
     */
    public function prePersist(PrePersistEvent $event): void {
        $this->countryService->checkExistingCountry($event);
    }

}
