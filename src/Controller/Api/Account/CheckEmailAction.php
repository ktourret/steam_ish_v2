<?php

namespace App\Controller\Api\Account;

use App\Repository\UserRepository;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Symfony\Component\HttpFoundation\JsonResponse;

class CheckEmailAction
{

    /**
     * CollectionAction constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(private UserRepository $userRepository) { }

    /**
     * Get User account by slug
     * @param string $email
     * @return JsonResponse
     */
    #[Get('/api/user/checkExistingEmail/{email}')]
    public function __invoke(string $email): JsonResponse
    {
        $exists = false;

        if (null !== $this->userRepository->findOneBy(['email' => $email])) {
            $exists = true;
        }

        return new JsonResponse(['exists' => $exists]);
    }
}
