<?php

namespace App\Controller\Api\Category;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\NonUniqueResultException;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class GetAction
{
    /**
     * CollectionAction constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(private CategoryRepository $categoryRepository) { }

    /**
     * Get Genres by slug
     * @param string $slug
     * @return Category|null
     * @throws NonUniqueResultException
     */
    #[Get('/api/category/{slug}')]
    #[Serializable(groups: 'Category')]
    public function __invoke(string $slug): ?Category
    {
        return $this->categoryRepository->findOneBySlug($slug);
    }
}
