<?php

namespace App\Controller\Api\Category;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Doctrine\ORM\QueryBuilder;
use Drosalys\Bundle\ApiBundle\Pagination\Attributes\Paginable;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class CollectionAction
{
    /**
     * CollectionAction constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(private CategoryRepository $categoryRepository) { }

    /**
     * Get Genre list.
     * @return QueryBuilder
     */
    #[Get('/api/category')]
    #[Serializable(groups: 'CategoryList')]
    #[Paginable(Category::class)]
    public function __invoke(): QueryBuilder
    {
        return $this->categoryRepository->queryAll();
    }

}
