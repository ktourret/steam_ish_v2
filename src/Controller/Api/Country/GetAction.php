<?php

namespace App\Controller\Api\Country;

use App\Entity\Country;
use App\Repository\CountryRepository;
use Doctrine\ORM\NonUniqueResultException;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class GetAction
{

    /**
     * CollectionAction constructor.
     * @param CountryRepository $countryRepository
     */
    public function __construct(private CountryRepository $countryRepository) { }

    /**
     * Get Country by slug
     * @param string $slug
     * @return Country|null
     * @throws NonUniqueResultException
     */
    #[Get('/api/country/{slug}')]
    #[Serializable(groups: 'Country')]
    public function __invoke(string $slug): ?Country
    {
        return $this->countryRepository->findOneBySlug($slug);
    }

}