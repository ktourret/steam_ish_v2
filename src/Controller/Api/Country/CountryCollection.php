<?php

namespace App\Controller\Api\Country;

use App\Entity\Country;
use App\Repository\CountryRepository;
use Doctrine\ORM\QueryBuilder;
use Drosalys\Bundle\ApiBundle\Pagination\Attributes\Paginable;
use Drosalys\Bundle\ApiBundle\Routing\Attributes\Get;
use Drosalys\Bundle\ApiBundle\Serializer\Attributes\Serializable;

class CountryCollection
{
    /**
     * CollectionAction constructor.
     * @param CountryRepository $countryRepository
     */
    public function __construct(private CountryRepository $countryRepository) { }

    /**s
     * Get Country list.
     * @return QueryBuilder
     */
    #[Get('/api/country')]
    #[Serializable(groups: 'CountryList')]
    #[Paginable(Country::class)]
    public function __invoke(): QueryBuilder
    {
        return $this->countryRepository->queryAll();
    }

}
