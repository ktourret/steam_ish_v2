<?php

namespace App\Form\Api;

use Drosalys\Bundle\ApiBundle\Filter\ApiFilter;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\DateFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CommentFilterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('createdAt', DateFilterType::class, [
                'choice_name' => 'name',
                'condition_pattern' => FilterOperands::STRING_CONTAINS,
            ])
        ;
    }

    public function getParent(): string
    {
        return ApiFilter::class;
    }
}