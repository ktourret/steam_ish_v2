import {typeWatch} from "../../../shared/scripts/component/tyepwatch";
import {Spinner} from "../../../shared/scripts/component/spinner";
import {HTMLResponse} from "../../../shared/scripts/interface/html-response";

class AjaxSearchBar {

    readonly INPUT_SELECTOR = '[data-search-input]';
    readonly BTN_SELECTOR = '[data-search-button]';
    readonly RESULT_CONTAINER_SELECTOR = '[data-search-result-container]';

    private readonly inputSearch: HTMLInputElement;
    private readonly buttonSearch: HTMLButtonElement;
    private readonly body: HTMLBodyElement;
    private readonly resultContainer: HTMLUListElement;
    private ongoingSearched: boolean;

    constructor(
        private element: HTMLElement,
        private url: string,
        private urlSearch: string,
    ) {
        this.body = document.querySelector('body');

        this.inputSearch = this.element.querySelector(this.INPUT_SELECTOR);
        this.buttonSearch = this.element.querySelector(this.BTN_SELECTOR);
        this.resultContainer = this.element.querySelector(this.RESULT_CONTAINER_SELECTOR);
        if (!this.isIntegrityIntact()) {
            console.error("Couldn't initialize Search Bar. A components is missing.")
            return;
        }
        this.ongoingSearched = false;
        this.initInput();
        this.initSearch();
    }

    private isIntegrityIntact(): boolean {
        return null != this.inputSearch
            && null != this.buttonSearch
            && null != this.resultContainer;
    }

    private initInput(): void {
        this.inputSearch.addEventListener('keyup', () => {
            typeWatch(() => {
                this.sendAjaxCall();
            }, 500);
        });
        this.inputSearch.addEventListener('click', () => {
            if (this.inputSearch.value.length >= 2) {
                this.showSearch();
            }
        });
    }

    private showSearch() {
        this.resultContainer.classList.remove('d-none');
        const navbar: HTMLElement = document.querySelector('nav.navbar-header');
        const nextSibling: Element = navbar?.nextElementSibling;
        if (nextSibling != null && !document.querySelector('[data-search-backdrop]')) {
            nextSibling.innerHTML = this.createBackdrop() + nextSibling.innerHTML;
        }
        if (!this.body.classList.contains('modal-open')) {
            this.body.classList.toggle('modal-open');
        }
        document.addEventListener('click', (event) => {
            if (event.target !== this.resultContainer && event.target !== this.inputSearch) {
                this.resultContainer.classList.add('d-none');
                if (this.body.classList.contains('modal-open')) {
                    this.body.classList.remove('modal-open');
                }
                const backdrop: HTMLDivElement = document.querySelector('[data-search-backdrop]');
                if (backdrop) {
                    backdrop.remove();
                }
            }
        });
    }

    private initSearch(): void {
        this.buttonSearch.addEventListener('click', () => {
            if (this.inputSearch.value) {
                window.location.pathname = this.urlSearch + this.inputSearch.value;
            }
        });
    }

    private createBackdrop(): string {
        return '<div class="modal-backdrop fade show" data-search-backdrop></div>';
    }

    private sendAjaxCall() {
        const content: string = this.inputSearch.value;
        if (content.length >= 2 && !this.ongoingSearched) {
            this.ongoingSearched = true;
            const spinner = new Spinner();
            spinner.start();
            console.log(this.url + content)
            fetch(this.url + content, {method: 'GET'})
                .then((response: Response) => {
                    return response.json();
                })
                .then((data: HTMLResponse) => {
                        if (data) {
                            this.resultContainer.innerHTML = data.html;
                            this.showSearch();
                            this.ongoingSearched = false;
                        }
                    }
                )
                .finally(() => {
                    spinner.close();
                });
        } else {
            this.resultContainer.classList.add("d-none");
        }
    }
}

window.addEventListener('load', () => {
    const headerSearchBar = document.querySelector<HTMLElement>("[data-container-search]");
    if (null === headerSearchBar) {
        return;
    }
    new AjaxSearchBar(
        headerSearchBar,
        '/ajax/searched-item/',
        '/recherche'
    );
});
