export function initAlert() {
    const alertContainer = document.querySelector('[data-alert]');
    for(let i = 0; i < alertContainer.children.length; i++) {
        mySetTimeOut(alertContainer.children[i]);
    }
}

export function mySetTimeOut(element: Element) {
    setTimeout(() => {
        element.classList.add('fade');
        setTimeout(() => {
            element.remove();
        }, 800);
    }, 4000);
}

window.addEventListener('load', () => {
    initAlert();
});
