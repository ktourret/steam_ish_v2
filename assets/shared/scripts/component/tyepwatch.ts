export let typeWatch = (() => {
    let timer: any = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    }
})();
