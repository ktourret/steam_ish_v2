
export class Spinner {

    private readonly body: HTMLBodyElement = document.querySelector('body');

    start(): void {
        if (this.body) {
            this.body.classList.add('position-relative');
            this.body.prepend(this.generateSpinner());
        }
    }

    close(): void {
        if (this.body) {
            this.body.classList.remove('position-relative');
            const async = document.querySelector('.async-spinner');
            async.remove();
        }
    }

    private generateSpinner(): HTMLDivElement {
        const mainDiv = document.createElement('div');
        mainDiv.classList.add('async-spinner');
        const divCenter = document.createElement('div');
        divCenter.classList.add('position-relative');
        divCenter.classList.add('d-flex');
        divCenter.classList.add('justify-content-center');
        divCenter.style.marginTop = '30vh';
        const div = document.createElement('div');
        div.classList.add('spinner-border');
        div.style.width = '6em';
        div.style.height = '6em';
        div.style.color = 'white';
        div.setAttribute('role', 'status');
        const span = document.createElement('span');
        span.classList.add('visually-hidden');
        divCenter.appendChild(div);
        div.appendChild(span);
        mainDiv.appendChild(divCenter);
        return mainDiv;
    }

}

