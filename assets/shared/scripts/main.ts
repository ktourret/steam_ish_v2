
import 'bootstrap';
import '@popperjs/core';

import './component/collection-form';
import './component/alert';
import './component/multiple-select';
