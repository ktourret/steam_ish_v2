#!/bin/bash

GID=`id -G | cut -f1 -d ' '`

# Get stack path & move into it.
STACK_PATH=`realpath "$(dirname $0)"`
echo -n "Change directory to $STACK_PATH..."
cd $STACK_PATH
echo " Done."

# Check if in stack.
if [[ ! -f docker-compose.yaml ]] ; then
	1>&2 echo "Do not find any ./docker-compose.sh.yml in the $(dirname $0) directory."
	1>&2 echo "Are you sure you have placed this $0 script inside a stack directory."
	exit 1
fi

# Rebuild php docker image
echo -n "Rebuild php docker image..."
./docker-compose.sh build

if [ $? -eq 0 ]; then
	echo " OK."
else
	echo " KO."
	1>&2 echo "Cannot build php docker image."
	exit 5
fi

./docker-compose.sh down
./docker-compose.sh up -d

# Clear Cache
echo -n "Clear cache..."
./docker-compose.sh exec -T php php bin/console cache:clear -n

if [ $? -eq 0 ]; then
	echo " OK."
else
	echo " KO."
	1>&2 echo "Cannot clear cache."
	exit 2
fi

# Apply migration
echo -n "Apply migration..."
./docker-compose.sh exec -T php php bin/console doctrine:migrations:migrate -n

if [ $? -eq 0 ]; then
	echo " OK."
else
	echo " KO."
	1>&2 echo "Cannot apply migrations in database."
	exit 3
fi

# Clear Cache
echo -n "Clear cache..."
./docker-compose.sh exec -T php php bin/console cache:clear -n

if [ $? -eq 0 ]; then
	echo " OK."
else
	echo " KO."
	1>&2 echo "Cannot clear cache."
	exit 2
fi

if [ "fixtures" = "$1" ] ; then
  ./docker-compose.sh exec -T php php bin/console hautelook:fixtures:load --purge-with-truncate -n
fi
