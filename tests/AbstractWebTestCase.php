<?php


namespace App\Tests;


use App\Repository\GameRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\AbstractBrowser;

/**
 * Class AbstractWebTestCase.php
 *
 * @author Kevin Tourret
 */
class AbstractWebTestCase extends WebTestCase
{

    protected static string $HOME = '/';
    protected static string $GAME = '/game/';

    protected function getClient(): KernelBrowser|AbstractBrowser|null
    {
        return static::createClient();
    }

    protected function getGameRepository(): GameRepository
    {
        return $this->getContainer()->get('doctrine')->getRepository('App:Game');
    }
}
