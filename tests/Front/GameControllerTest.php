<?php


namespace App\Tests\Front;


use App\Tests\AbstractWebTestCase;

/**
 * Class GameControllerTest.php
 *
 * @author Kevin Tourret
 */
class GameControllerTest extends AbstractWebTestCase
{

    public function testAllGames()
    {
        $client = $this->getClient();
        $gameRepository = $this->getGameRepository();
        $games = $gameRepository->findAll();

        foreach ($games as $game) {
            $client->request('GET', self::$GAME . $game->getSlug());
            $this->assertResponseStatusCodeSame(200, 'Error getting the game page : ' . $game->getSlug());
        }
    }

}
