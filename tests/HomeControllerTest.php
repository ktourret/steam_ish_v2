<?php

namespace App\Tests;

use Symfony\Component\DomCrawler\Crawler;

class HomeControllerTest extends AbstractWebTestCase
{

    public function testHomePageAccess()
    {
        $client = static::createClient();
        $client->request('GET', self::$HOME);
        $this->assertResponseStatusCodeSame(200, 'Error getting the home page');
    }

    public function testHomePageTitle()
    {
        $client = static::createClient();
        $crawlerGlobal = $client->request('GET', self::$HOME);

        $crawler = $crawlerGlobal->filter('h2');

        $this->assertCount(4, $crawler, "One title is missing");
    }

    public function testHomeTitleAllGame() {
        $client = static::createClient();
        $crawlerGlobal = $client->request('GET', self::$HOME);
        $this->checkTitleValue('Tous les jeux', $crawlerGlobal);
    }

    public function testHomeTitleLastOut() {
        $client = static::createClient();
        $crawlerGlobal = $client->request('GET', self::$HOME);
        $this->checkTitleValue('Les dernières sorties', $crawlerGlobal);
    }

    public function testHomeTitleLastComments() {
        $client = static::createClient();
        $crawlerGlobal = $client->request('GET', self::$HOME);
        $this->checkTitleValue('Les derniers commentaires', $crawlerGlobal);
    }

    public function testHomeTitleMostPlayed() {
        $client = static::createClient();
        $crawlerGlobal = $client->request('GET', self::$HOME);
        $this->checkTitleValue('Les plus joués', $crawlerGlobal);
    }

    public function checkTitleValue(string $title, Crawler $crawler) {
        $this->assertStringContainsString( $title, $crawler->text(), 'Title "' . $title .'" not found');
    }

}
