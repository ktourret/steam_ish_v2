###
# Base PHP
###
FROM php:8.0-fpm AS php-base

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && \
    install-php-extensions \
        bz2 \
        exif \
        gd \
        imap \
        intl \
        opcache \
        pdo_mysql \
        tidy \
        xmlrpc \
        xsl \
        zip

WORKDIR /var/www/html

RUN rm -Rf /usr/local/bin/install-php-extensions
COPY config/php.ini "$PHP_INI_DIR/php.ini"
